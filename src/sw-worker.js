
var cacheName = 'ltms-pwa-feed-cache';
var dataCacheName = 'ltms-pwa-feed-data';

var filesToCache = [
  './index.html',
];

self.addEventListener('fetch', event => {
  console.log('[Service Worker] Fetching something ....', event.request.url);
  event.respondWith(
    caches.match(event.request)
    .then(function(response) {
      return response || fetchAndCache(event.request);
    })
  );
});

function fetchAndCache(url) {
  return fetch(url)
  .then(function(response) {
    // Check if we received a valid response
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return caches.open(cacheName)
    .then(function(cache) {
      cache.put(url, response.clone());
      return response;
    });
  })
  .catch(function(error) {
    console.log('Request failed:', error);
    // You could return a custom offline 404 page here
  });
}

 self.addEventListener('install', function(event) {
   console.log('[Service Worker] Installing Service Worker ...', event);
   event.waitUntil(
    caches.open(cacheName).then(cache => {
      console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache);
    })
  );
 });

 self.addEventListener('activate', function(event) {
   console.log('[Service Worker] Activating Service Worker ....', event);
   return self.clients.claim();
 });

 self.addEventListener('fetch', function(event) {
   console.log('[Service Worker] Fetching something ....', event);
 });

 self.addEventListener('push', function(e) {
   console.log('[ServiceWorker] push');
   var data = e.data.json();
   e.waitUntil(
     self.registration.showNotification(data['notification'].title, data['notification'])
   );
 });

 self.addEventListener('notificationclick', function(event) {
   console.log('On notification click: ', event.notification.tag);
   
   event.notification.close();
   // This looks to see if the current is already open and  
   // focuses if it is  
   event.waitUntil(
     clients.matchAll({
       type: "window"
     })
     .then(function(clientList) {
       for (var i = 0; i < clientList.length; i++) {
         var client = clientList[i];
         if (client.url == '/' && 'focus' in client)
           return client.focus();
       }
       if (clients.openWindow) {
         var feed_id = event.notification.data.feed_id;

         return clients.openWindow('https://staging.litmusworld.com/rateus/pwa/index.html?feed_id=' + feed_id);
       }
     })
   );
 });

// self.addEventListener('notificationclick', function(event) {
//   console.log('On notification click: ', event.notification.tag);
//   event.notification.close();
//   // This looks to see if the current is already open and  
//   // focuses if it is  
//   event.waitUntil(
//     clients.matchAll({
//       type: "window"
//     })
//     .then(function(clientList) {
//       for (var i = 0; i < clientList.length; i++) {
//         var client = clientList[i];
//         if (client.url == '/' && 'focus' in client)
//           return client.focus();
//       }
//       if (clients.openWindow) {
//         feed_id = '5b64492d8c05761eedc4fa63'

//         return clients.openWindow('http://localhost:4200/feed-detail/' + feed_id);
//       }
//     })
//   );
// });
