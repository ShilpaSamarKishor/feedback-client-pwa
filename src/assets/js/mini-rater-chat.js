! function() {
  function t(t, r) {
    function s(t, l, n, e, a, i) { var h = t * Math.PI,
        o = l * Math.PI;
      d.save(), d.beginPath(), d.arc(b, v, p, h, o, i), d.lineWidth = a, d.globalAlpha = e, d.strokeStyle = n, d.stroke(), d.globalAlpha = 1, d.restore() }

    function u() { s(1.5, 1.88, g, .2, w, !1), s(1.9, 2.28, g, .2, w, !1), s(2.3, 2.68, g, .2, w, !1), s(2.7, 3.08, g, .2, w, !1), s(3.1, 3.48, g, .2, w, !1), d.beginPath(); var t = n ? n : 12;
      d.arc(b, v, t, 0 * Math.PI, 2 * Math.PI, !1), d.fillStyle = "white", d.fill(), d.lineWidth = 1, d.globalAlpha = 1, d.strokeStyle = "white", d.stroke(), d.globalAlpha = 2 }

    function c(t) { P && (t >= -.5 && -.1 > t && s(-.5, t, h[0], 1, w, !1), t >= -.1 && .3 > t && (s(-.5, -.123, h[1], 1, w, !1), s(-.1, t, h[1], 1, w, !1)), t >= .3 && .7 > t && (s(-.5, -.123, h[2], 1, w, !1), s(-.1, .28, h[2], 1, w, !1), s(.3, t, h[2], 1, w, !1)), t >= .7 && 1.1 > t && (s(-.5, -.123, h[3], 1, w, !1), s(-.1, .28, h[3], 1, w, !1), s(.3, .68, h[3], 1, w, !1), s(.7, t, h[3], 1, w, !1)), t >= 1.1 && 1.5 > t && (s(-.5, -.123, h[4], 1, w, !1), s(-.1, .28, h[4], 1, w, !1), s(.3, .68, h[4], 1, w, !1), s(.7, 1.08, h[4], 1, w, !1), s(1.1, t, h[4], 1, w, !1))), P || (t >= 1.1 && 1.49 > t && s(-.52, t, o[0], 1, w, !0), t >= .7 && 1.1 > t && (s(-.52, 1.12, o[1], 1, w, !0), s(1.1, t, o[1], 1, w, !0)), t >= .3 && .7 > t && (s(-.52, 1.12, o[2], 1, w, !0), s(1.1, .72, o[2], 1, w, !0), s(.7, t, o[2], 1, w, !0)), t >= -.1 && .3 > t && (s(-.52, 1.12, o[3], 1, w, !0), s(1.1, .72, o[3], 1, w, !0), s(.7, .32, o[3], 1, w, !0), s(.3, t, o[3], 1, w, !0)), t >= -.5 && -.1 > t && (s(-.52, 1.12, o[4], 1, w, !0), s(1.1, .72, o[4], 1, w, !0), s(.7, .32, o[4], 1, w, !0), s(.3, -.08, o[4], 1, w, !0), s(-.1, t, o[4], 1, w, !0))) }

    function f(t) { d.beginPath(), d.font = a ? a : "bold 12pt Open Sans", d.fillStyle = "#000", d.textAlign = "center"; var l = a ? 4 : 5;
      t > 0 && (t = t), i && i > 15 ? l += 10 : a && 12 >= i && (l += 3), d.fillText(t, b, v + l) } var d = t.getContext("2d"),
      g = "#20314D",
      b = t.width / 2,
      v = t.height / 2,
      p = l ? l : 20,
      P = (2 * Math.PI, null),
      w = e ? e : 8;
    d.clearRect(0, 0, t.width, t.height); var I = -1;
    r > 0 ? (P = !0, I = -.51 + .4 * r) : (P = !1, I = 1.5 + .4 * r), u(), c(I), f(r) } var l = null,
    n = null,
    e = null,
    a = null,
    i = !1,
    h = ["#f5ea14", "#bbd531", "#66b846", "#0b7139", "#193419"],
    o = ["#fab416", "#f48020", "#f05f22", "#ed2424", "#c42031"];
  MiniRater = function(r, s, u) { l && n && e && l && (l = null, n = null, e = null, a = null), s && (h = s), u && (o = u), this.canvas = document.getElementById(r), this.drawWheel = t, this.draw = function() { this.drawWheel(this.canvas, 0) }, this.setRating = function(t) { this.drawWheel(this.canvas, t) }, this.setRadius = function(t, h, o, r) { l = t, n = h, e = o, a = "bold " + r + "pt Open Sans", i = r } } }(),
function() {
  function t(t, h) {
    function o(t, l, n, e, a, i) { var h = t * Math.PI,
        o = l * Math.PI;
      c.save(), c.beginPath(), c.arc(g, b, v, h, o, i), c.lineWidth = a, c.globalAlpha = e, c.strokeStyle = n, c.stroke(), c.globalAlpha = 1, c.restore() }

    function r() { o(1.51, 1.672, d, .6, p, !1), o(1.692, 1.854, d, .6, p, !1), o(1.874, 2.036, d, .6, p, !1), o(2.056, 2.218, d, .6, p, !1), o(2.238, 2.4, d, .6, p, !1), o(2.42, 2.582, d, .6, p, !1), o(2.602, 2.764, d, .6, p, !1), o(2.784, 2.946, d, .6, p, !1), o(2.966, 3.128, d, .6, p, !1), o(3.148, 3.31, d, .6, p, !1), o(3.33, 3.49, d, .6, p, !1), c.beginPath(); var t = n ? n : 12;
      c.arc(g, b, t, 0 * Math.PI, 2 * Math.PI, !1), c.fillStyle = "white", c.fill(), c.lineWidth = 1, c.globalAlpha = 1, c.strokeStyle = "white", c.stroke(), c.globalAlpha = 2 }

    function s(t) { t >= -.49 && -.304 > t && o(-.49, t, f[0], 1, p, !1), t >= -.304 && -.126 > t && (o(-.49, -.328, f[0], 1, p, !1), o(-.308, t, f[0], 1, p, !1)), t >= -.126 && .056 > t && (o(-.49, -.328, f[0], 1, p, !1), o(-.308, -.146, f[0], 1, p, !1), o(-.126, t, f[0], 1, p, !1)), t >= .056 && .238 > t && (o(-.49, -.328, f[0], 1, p, !1), o(-.308, -.146, f[0], 1, p, !1), o(-.126, .036, f[0], 1, p, !1), o(.056, t, f[0], 1, p, !1)), t >= .238 && .42 > t && (o(-.49, -.328, f[0], 1, p, !1), o(-.308, -.146, f[0], 1, p, !1), o(-.126, .036, f[0], 1, p, !1), o(.056, .218, f[0], 1, p, !1), o(.238, t, f[0], 1, p, !1)), t >= .42 && .602 > t && (o(-.49, -.328, f[0], 1, p, !1), o(-.308, -.146, f[0], 1, p, !1), o(-.126, .036, f[0], 1, p, !1), o(.056, .218, f[0], 1, p, !1), o(.238, .4, f[0], 1, p, !1), o(.42, t, f[0], 1, p, !1)), t >= .602 && .784 > t && (o(-.49, -.328, f[0], 1, p, !1), o(-.308, -.146, f[0], 1, p, !1), o(-.126, .036, f[0], 1, p, !1), o(.056, .218, f[0], 1, p, !1), o(.238, .4, f[0], 1, p, !1), o(.42, .582, f[0], 1, p, !1), o(.602, t, f[0], 1, p, !1)), t >= .784 && .966 > t && (o(-.49, -.328, f[1], 1, p, !1), o(-.308, -.146, f[1], 1, p, !1), o(-.126, .036, f[1], 1, p, !1), o(.056, .218, f[1], 1, p, !1), o(.238, .4, f[1], 1, p, !1), o(.42, .582, f[1], 1, p, !1), o(.602, .764, f[1], 1, p, !1), o(.784, t, f[1], 1, p, !1)), t >= .966 && 1.148 > t && (o(-.49, -.328, f[1], 1, p, !1), o(-.308, -.146, f[1], 1, p, !1), o(-.126, .036, f[1], 1, p, !1), o(.056, .218, f[1], 1, p, !1), o(.238, .4, f[1], 1, p, !1), o(.42, .582, f[1], 1, p, !1), o(.602, .764, f[1], 1, p, !1), o(.784, .946, f[1], 1, p, !1), o(.966, t, f[1], 1, p, !1)), t >= 1.148 && 1.33 > t && (o(-.49, -.328, f[2], 1, p, !1), o(-.308, -.146, f[2], 1, p, !1), o(-.126, .036, f[2], 1, p, !1), o(.056, .218, f[2], 1, p, !1), o(.238, .4, f[2], 1, p, !1), o(.42, .582, f[2], 1, p, !1), o(.602, .764, f[2], 1, p, !1), o(.784, .946, f[2], 1, p, !1), o(.966, 1.128, f[2], 1, p, !1), o(1.148, t, f[2], 1, p, !1)), t >= 1.33 && 1.512 > t && (o(-.49, -.328, f[2], 1, p, !1), o(-.308, -.146, f[2], 1, p, !1), o(-.126, .036, f[2], 1, p, !1), o(.056, .218, f[2], 1, p, !1), o(.238, .4, f[2], 1, p, !1), o(.42, .582, f[2], 1, p, !1), o(.602, .764, f[2], 1, p, !1), o(.784, .946, f[2], 1, p, !1), o(.966, 1.128, f[2], 1, p, !1), o(1.148, 1.31, f[2], 1, p, !1), o(1.33, t, f[2], 1, p, !1)) }

    function u(t) { c.beginPath(), c.font = a ? a : "bold 12pt Roboto", c.fillStyle = "#000", c.textAlign = "center"; var l = 5;
      i && i > 15 ? l += 10 : a && 12 >= i && (l += 2), c.fillText(t, g, b + l) } var c = t.getContext("2d"),
      f = ["red", "orange", "green"],
      d = "#20314D",
      g = t.width / 2,
      b = t.height / 2,
      v = l ? l : 20,
      p = (2 * Math.PI, e ? e : 8);
    c.clearRect(0, 0, t.width, t.height); var P = -1;
    r(), u(h ? h : ""), h = parseFloat(h) + parseFloat("1.0"), P = -.49 + .18 * h, s(P) } var l = null,
    n = null,
    e = null,
    a = null,
    i = !1;
  NPSMiniRater = function(h) { l && n && e && l && (l = null, n = null, e = null, a = null), this.canvas = document.getElementById(h), this.drawWheel = t, this.draw = function() { this.drawWheel(this.canvas, 0) }, this.setRating = function(t) { this.drawWheel(this.canvas, t) }, this.setRadius = function(t, h, o, r) { l = t, n = h, e = o, a = "bold " + r + "pt Open Sans", i = r } } }();
