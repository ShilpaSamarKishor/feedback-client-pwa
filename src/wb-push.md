


## How To show the Allow Notifications Dialog

If by accident we click "Deny" in the Allow Notifications dialog after hitting subscribe,

In order to trigger again the Allow Notifications popup, we need first to clear localhost from this list - [chrome://settings/content/notifications](chrome://settings/content/notifications)


## Generating VAPID keys

In order to generate a public/private VAPID key pair, we first need to install the [web-push] library globally:


    npm install web-push -g


We can then generate a VAPID key pair with this command:

    web-push generate-vapid-keys --json

And here is a sample output of this command:

```json
   {"publicKey":"BLbP49d3f9w8pRYl0P7Lt5WfIhUoP60v4saeQroCgWd6AVdPJhSyIQn-rJ18OB9JvepSABbj7y9jWpkdj1V-6VE",
   	"privateKey":"lEYeJeEg0faiAMWsLf6pUncu1BRmOFeLYa_MSS2KFT0"
   }
```


For build => ng build --prod
For run => http-server -p 8080 -c-1 dist/feed-back
run => http://localhost:8080/



