import { Component, OnInit } from '@angular/core';
import { UserService } from './feed-services/user.service';
import { BrandService } from './feed-services/brand.service';
import { LocalStorageService } from './feed-services/localstorage.service';
import { FeedService } from './feed-services/feed.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'LW Help Desk';

  constructor(private userService : UserService, private brandService : BrandService,private feedService : FeedService  ,private localStorageService : LocalStorageService  ){ }

  ngOnInit() {
        this.userService.getUserDetails().subscribe(data => {
          console.log("Saved user details.");
        });  

        this.brandService.getBrandDetails().subscribe(data => {
          console.log("Saved brand details.");
        });
  }
}
