import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalStorageService } from '../feed-services/localstorage.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  constructor(private router : Router,private activatedRoute: ActivatedRoute,
  private localStorageService : LocalStorageService, private spinner: NgxSpinnerService) { }

  userProfile : any = "";
  profilePicUrl ;

  ngOnInit() {
      this.spinner.show();
      this.userProfile =  this.localStorageService.getValue("user");
      this.profilePicUrl = this.localStorageService.getValue("userProfilePic");
      if(this.userProfile){
           this.userProfile = JSON.parse(this.userProfile);
           this.spinner.hide();

       }
      var feedId = this.activatedRoute.snapshot.queryParams['feed_id'];
      if(feedId && feedId != "undefined"){
        this.router.navigateByUrl('/feed-detail/' + feedId);  
      }    
    	    
  }

  renderEditUserProfile(){
  	  this.router.navigateByUrl('/edit-user-profile');
  }
}
