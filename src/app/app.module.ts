import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FeedListComponent } from './feed-list/feed-list.component';
import { FeedItemComponent } from './feed-item/feed-item.component';
import { AppRoutingModule } from './/app-routing.module';
import { HttpClientModule }    from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FeedDetailComponent } from './feed-detail/feed-detail.component';
import { TimeAgoPipe } from 'time-ago-pipe';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { PushNotificationComponent } from './push-notification/push-notification.component';
import { FeedHeaderComponent } from './feed-header/feed-header.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ServiceRequestComponent } from './service-request/service-request.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { EditUserProfileComponent } from './edit-user-profile/edit-user-profile.component';
import { ProjectItemComponent } from './project-item/project-item.component';
import { FormsModule }   from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { WebcamModule } from 'ngx-webcam';


@NgModule({
  declarations: [
    AppComponent,
    FeedListComponent,
    FeedItemComponent,
    FeedDetailComponent,
    TimeAgoPipe,
    PushNotificationComponent,
    FeedHeaderComponent,
    ProjectListComponent,
    ServiceRequestComponent,
    UserProfileComponent,
    EditUserProfileComponent,
    ProjectItemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    FormsModule,
    NgxSpinnerModule,
    WebcamModule,
    //ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    //ServiceWorkerModule.register('/sw-worker.js', { enabled: environment.production })
    ServiceWorkerModule.register('https://staging.litmusworld.com/rateus/pwa/ngsw-worker.js', { enabled: environment.production })  
  ],
  providers: [],
  bootstrap: [AppComponent]
})



export class AppModule {

 }
