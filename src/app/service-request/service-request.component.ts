import { Component, OnInit, Input, ViewChild, ElementRef, } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { GenerateRequestService } from '../feed-services/generate-request.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-service-request',
  templateUrl: './service-request.component.html',
  styleUrls: ['./service-request.component.css']
})
export class ServiceRequestComponent implements OnInit {
  @ViewChild("ltmsPreviewContainer") ltmsPreviewContainer: ElementRef;
  ContanierHeight: any;
  ContanierWidth: any;

  feedbackUrl = {
      "longUrl" : 'https://litmusworld.com'
  }
  constructor(private generateRequestService : GenerateRequestService, private route: ActivatedRoute,private sanitizer: DomSanitizer, private spinner: NgxSpinnerService) { 
     this.ContanierHeight = (window.screen.height)-51 + "px";
     this.ContanierWidth = (window.screen.width) + "px";
  }
  
  ngOnInit() {
    this.spinner.show();
		this.getFeedbackUrl()
    this.ltmsPreviewContainer.nativeElement.style.height = this.ContanierHeight;
  }

  //get feedback request url from api
  getFeedbackUrl(){
      this.route.params.subscribe(params => {
       this.generateRequestService.getFeedbackRequestUrl(params.app_id , (response) => {
          setTimeout(()=>{
              this.spinner.hide();
          },2000);
          this.feedbackUrl.longUrl = response['data']['long_url'];
       })
    })
  }

  // set safe url for i frame using DomSanitizer
  setFeedbackUrl(Url : string){
  	if(!Url) {return null;}
    else{
      return this.sanitizer.bypassSecurityTrustResourceUrl(Url);
    } 
  }
}