import { Component, OnInit, Input } from '@angular/core';
import { GenerateRequestService } from '../feed-services/generate-request.service';
import { Router } from '@angular/router';
import { ServiceRequestComponent } from '../service-request/service-request.component';

@Component({
  selector: 'app-project-item',
  templateUrl: './project-item.component.html',
  styleUrls: ['./project-item.component.css']
})
export class ProjectItemComponent implements OnInit {

  constructor(private generateRequestService : GenerateRequestService, private router : Router) { }


  @Input() shop;

  ngOnInit() {

  }

  //redirect the page to service request 
  generateAndRedirect(){
      this.router.navigateByUrl('/service-request/'+this.shop.app_id);
  }
}
