import { Component, OnInit } from '@angular/core';
import { FeedService } from '../feed-services/feed.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ShopService } from '../feed-services/shop.service';
import { Router,ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-feed-list',
  templateUrl: './feed-list.component.html',
  styleUrls: ['./feed-list.component.css']
})
export class FeedListComponent implements OnInit {
  feeds = [];
  public shops = [];

  constructor(private feedService: FeedService, private spinner: NgxSpinnerService,
    private shopService : ShopService,
    private router : Router,
    private activatedRoute: ActivatedRoute,) {
  }

  ngOnInit() {
    var feedId = this.activatedRoute.snapshot.queryParams['feed_id'];
    if(feedId && feedId != "undefined"){
      this.router.navigateByUrl('/feed-detail/' + feedId);  
    }   
    this.spinner.show();
    this.getFeedBacks();
    this.getShopData(); 
  }
  
  getShopData(){
    this.shopService.getShops().subscribe(data => {
         this.shops= data;
         this.spinner.hide();
      });
  }

   //redirect the page to service request 
  generateAndRedirect(){
      this.router.navigateByUrl('/service-request/gli2_complaint');
  }

  //Get Customise feed for display feed list
  getFeedBacks(): void {
    this.feedService.getFeedBacks().subscribe(data => {
      if(data['feed']){
        data['feed'].forEach(feed => {
        
        let feedItem = feed;
        feedItem.className = this.renderDeviceIcon(feed);

        data['shops'].forEach(shopname => {
          feedItem.shops_name = shopname['name'];
        })
        data['users'].forEach(username =>{
          if(feed['feed_by_id']==username['_id']){
            feedItem.users_name = username['name']
          }
        })

        this.feeds.push(feedItem); 
        this.spinner.hide();

      })
      }
      
    });
  }
   renderDeviceIcon(feed){
      if(feed['device']=='Other'){
        return "fa fa-external-link-alt fa-lg";
      }else if(feed['device']=='Phone'){
        return "fa fa-mobile fa-lg";
      }else if(feed['device']=='Mail'){
        return "fa fa-envelope fa-lg";
      }else{
        return null;
      }
    }
}
