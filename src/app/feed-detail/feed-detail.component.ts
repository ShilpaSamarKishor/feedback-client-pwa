import { Component, OnInit, AfterContentInit, ViewChild, ElementRef, AfterViewChecked, ViewEncapsulation, ChangeDetectorRef} from '@angular/core';
import { Location } from '@angular/common';
import { FeedDetailService } from '../feed-services/feed-detail.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { PostMessageService } from '../feed-services/post-message.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FileAttachmentService } from '../feed-services/file-attachment.service';
import { AttachmentPreviewService } from '../feed-services/attachment-preview.service';
import { StatusListService } from '../feed-services/status-list.service';
//Camera Capture
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';

import 'src/assets/js/mini-rater-chat.js';
import 'src/assets/js/toast.js';

declare var MiniRater : any;
declare var NPSMiniRater : any;
declare var toast : any;

@Component({
  selector: 'app-feed-detail',
  templateUrl: './feed-detail.component.html',
  styleUrls: ['./feed-detail.component.css']
})

export class FeedDetailComponent implements OnInit,  AfterContentInit {

  @ViewChild("ltmsPwaMessageInput") ltmsPwaMessageInput: ElementRef;
  @ViewChild("ltmsPwaDiscussion") ltmsPwaDiscussion:ElementRef;
  // file attachment
  @ViewChild("ltmsPwaFileAttachmentInput") ltmsPwaFileAttachmentInput : ElementRef;
  @ViewChild("ltmsPwaFileAttachmentComponent") ltmsPwaFileAttachmentComponent:ElementRef;
  @ViewChild("ltmsPwaMessageInputedCaption") ltmsPwaMessageInputedCaption:ElementRef;
  @ViewChild("previewAttachedFile") previewAttachedFile :ElementRef;
  //camera Capture 
  @ViewChild("webCam") webCam : ElementRef;
  @ViewChild("ltmsPwaImageCaptureContainer") ltmsPwaImageCaptureContainer : ElementRef;
  @ViewChild("ltmsPwaImageMessageCaption") ltmsPwaImageMessageCaption : ElementRef;

  fileList = [];
  files = [];
  fileNames = [];
  public feedDetail;
  public feedId;
  public imageSrc;
  public parameterRatings;
  public statusName = "Open"; 
  public statusColor = "black";
  public JSON;
  //Camera Capture
  public imageFile :File ;
  ContanierHeight: any;
  ContanierWidth: any;

  // toggle webcam on/off
  public submitted = true;
  public showWebcam = false;
  public allowCameraSwitch = true;
  public multipleWebcamsAvailable = false;
  public deviceId: string;

  public errors: WebcamInitError[] = [];
  // latest snapshot
  public webcamImage: WebcamImage = null;
  // webcam snapshot trigger
  private trigger: Subject<void> = new Subject<void>();
  // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
  private nextWebcam: Subject<boolean|string> = new Subject<boolean|string>();

  
  constructor(private location: Location, private route: ActivatedRoute,
   private feedDatailService : FeedDetailService, private router:Router, 
   private postmessageService : PostMessageService,
   private feedDetailService : FeedDetailService, private spinner: NgxSpinnerService, 
   private fileAttachmentService :FileAttachmentService, private ref: ChangeDetectorRef,
   private attachmentPreviewService : AttachmentPreviewService,
    private statusListService: StatusListService) {
    this.ContanierHeight = (window.screen.height)-250 + "px";
    this.ContanierWidth = (window.screen.width) + "px";
    this.JSON = JSON;

  }

  postComment(message, attachment){
    if(!message){
      toast('Comment can not be empty', 1000);
    }
    else{
      var messages = {
        feed_id: this.feedDetail.feed._id,
        is_user_app: true,
        comment_text: message, 
        attachments : attachment,
        user_id: this.feedDetail.feed.feed_by_id
      };
      this.postmessageService.postComment(messages, function(){
      });
      this.ltmsPwaMessageInput.nativeElement.value = "";
      this.feedDatailService.getFeedDetailById(this.feedDetail['feed']._id).subscribe(feedDetail =>{
      this.feedDetail = feedDetail;
      toast('Message sent!', 1000);
      this.fileList.splice(0,this.fileList.length);
      this.fileNames.splice(0, this.fileNames.length);
      this.initialiseFeed();
      this.scrollToBottom();
      });
    } 
  }     

  ngOnInit() {
    this.spinner.show();
    // this.refreshFeed();
    this.initialiseFeed();
   }

  getSelectedFeedDetail():void {
         this.route.params.subscribe(params => {
         this.feedDatailService.getFeedDetailById(params.feedId).subscribe(feedDetail => {
         this.feedDetail = feedDetail;

         var detailedRatings = feedDetail['feed']['feed_params']['detailed_rating'];
         var detailedFeedbackItem = feedDetail['detailed_feedback_items']

         var questionMap = {}; 

         detailedFeedbackItem.forEach(function(a,b){ if(a['field_id'] && a['text'] && !questionMap[a['field_id']]) {questionMap[a['field_id']] = a } })
         this.parameterRatings = []; 
         detailedRatings.forEach(function(detailedRating){
            var quetionFieldID = detailedRating['field_id'];
            var quesionRating = detailedRating['rating'];
            var quesionComment = detailedRating['comment'];
            
            var currentQuesDetails = questionMap[quetionFieldID];
            if(currentQuesDetails && currentQuesDetails['prop'] && currentQuesDetails['prop'][quesionRating]){
              var currentProp = currentQuesDetails['prop'][quesionRating];
              var questionRatingLabel = currentProp['label'];
              this.parameterRatings.push({
                "question" : currentQuesDetails['text'],
                "answer" : questionRatingLabel ? questionRatingLabel : quesionRating
              })
            } else if(currentQuesDetails && currentQuesDetails['type'] == "multi-select"){
              this.parameterRatings.push({
                "question" : currentQuesDetails['group_text'],
                "answer" : currentQuesDetails['text']
              });
            }
         }.bind(this));
        this.getFeedStatus(this.feedDetail.feed['brand_id']);

         console.log(this.parameterRatings);
         this.renderMiniRater(this.feedDetail['feed'], 'feedDetailRatingCanvas');
         this.spinner.hide();
         setTimeout(() => {
          this.scrollToBottom();
         })
        });
      });
  }

   ngAfterContentInit() {
    // contentChild is set after the content has been initialized
        this.scrollToBottom() 
       }

  initialiseFeed(){
    this.getSelectedFeedDetail();
    this.feedDatailService.subject.next({
      "currentPage" : "feed-detail"
    });
  }

  renderMiniRater(feed, DomStr){
    var ratingWidget  = null;
    var raterType = feed["main_rater_type"];
    if(raterType=='nps'){
        ratingWidget = new NPSMiniRater(DomStr);
    }else{
       ratingWidget = new MiniRater(DomStr);
    }
    ratingWidget.draw();
    ratingWidget.setRating(feed["feed_params"]["primary_rating"]);
  }

  scrollToBottom(): void {
      try {
          this.ltmsPwaDiscussion.nativeElement.scrollTop = this.ltmsPwaDiscussion.nativeElement.scrollHeight;
        } catch(err) { }                 
  }

  // ngAfterViewChecked() {        
  //     this.scrollToBottom();        
  // } 

  refreshFeed(){
    this.feedId = setInterval(()=>{ this.getSelectedFeedDetail() 
        console.log('Testing refresh', this.feedId);
    }, 10000);
  }
  //Attachment on chat
  selectFileForAttachment(){
    this.ltmsPwaFileAttachmentInput.nativeElement.click();
  }

  ngOnDestroy() {
    if (this.feedId) {
      clearInterval(this.feedId);
    }
  }

  // file attachment 
  addFileAttachment(){
      this.ltmsPwaFileAttachmentInput.nativeElement.click();
    }

    getFileAttachment(event){
      if(event.target.files && event.target.files.length){
          this.ltmsPwaFileAttachmentComponent.nativeElement.style.display="block";
          for (var i = 0; i < event.target.files.length; i++) { 
                this.files.push(event.target.files[i]);

                var fileInfo = {
                  'imageSrc' : '',
                  'file': event.target.files[i] 
                }
                const reader = new FileReader();
                reader.onload = e => {this.imageSrc = reader.result;
                   fileInfo.imageSrc = this.imageSrc;}
                reader.readAsDataURL(event.target.files[i]);
                this.fileNames.push(fileInfo);
                console.log(this.fileNames);
          }
      }   
    }

    resetAttachedFile(fileName){
      const index: number = this.fileNames.indexOf(fileName);
          if (index !== -1) {
            this.fileNames.splice(index, 1);
            this.files.splice(index, 1);
          }  
    }
    //Post file to get uploder Id
    postFileAttachment(message){
        var totalResponseReceived = 0;
        for (let file of this.files) {
            this.fileAttachmentService.getFileAttachmentUrl(file, (res)=> {
               totalResponseReceived++;
               if(res['data']['url'])
               {
                 var fileUrl = res['data']['url'];
                 this.fileList.push(fileUrl);
               } else {
                 var fileId = res['data']['file_id']
                 this.fileList.push(fileId);
               }    
               if(totalResponseReceived == this.files.length){
                  this.postMessageAfterGettingUrls.apply(this,[message])
               }           
            })
        }          
    }

    postMessageAfterGettingUrls(message){
        this.postComment(message, this.fileList);
        this.ltmsPwaMessageInputedCaption.nativeElement.value = "";
        this.ltmsPwaFileAttachmentComponent.nativeElement.style.display="none";
    }
    //Close Attachment model 
    closeFileAttachment(){
      this.ltmsPwaFileAttachmentComponent.nativeElement.style.display="none";
    }

    //Read attached File from chat
    ReadAttachedFile(fileInfo){

      console.log('Read Attachment for the file info ', fileInfo );
        this.attachmentPreviewService.getUrlForReadAttachFile(fileInfo ,(previewUrl)=> {
              window.open(previewUrl['file_url']);
        })
    }

    getUrl(fileInfo){
        this.attachmentPreviewService.getUrlForReadAttachFile(fileInfo ,(previewUrl)=> {
              return previewUrl['file_url'];
        })
    }


    //Camera attachment for chat attachment
    OpenCameraForAttachment(){
      ///open block
      this.spinner.show();
      this.submitted = false;
      this.webCam.nativeElement.style.height = this.ContanierHeight;
      this.webCam.nativeElement.style.width = this.ContanierWidth;
      
      WebcamUtil.getAvailableVideoInputs()
        .then((mediaDevices: MediaDeviceInfo[]) => {
          this.showWebcam = true;
          this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
          this.ltmsPwaImageCaptureContainer.nativeElement.style.display = "block";
      });
    } 
  public videoOptions: MediaTrackConstraints = {
    width: {ideal: 1024},
    height: {ideal: 576}
  };
  public triggerSnapshot(): void {
    this.submitted = true;
    this.trigger.next();
  }

  public handleInitError(error: WebcamInitError): void {
    this.errors.push(error);
  }

  public showNextWebcam(directionOrDeviceId: boolean|string): void {
    // true => move forward through devices
    // false => move backwards through devices
    // string => move to device with given deviceId
    this.nextWebcam.next(directionOrDeviceId);
  }
  //Push capture image in array
  public handleImage(webcamImage: WebcamImage): void {
    this.webcamImage = webcamImage;
  }

  public cameraWasSwitched(deviceId: string): void {
    this.deviceId = deviceId;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean|string> {
    return this.nextWebcam.asObservable();
  }

  public reCaptureImage(){
    this.webcamImage = null;
    this.ltmsPwaImageMessageCaption.nativeElement.value = "";
    this.submitted = false;
  }

  public sendImageforChat(message ){
   var fileUrl = [];
   this.imageToFileConvert();
    if(this.imageFile && message ){
        this.ltmsPwaImageMessageCaption.nativeElement.value = "";
        this.ltmsPwaImageCaptureContainer.nativeElement.style.display = "none";
        this.fileAttachmentService.getFileAttachmentUrl(this.imageFile, (res)=> {
            fileUrl.push(res['data']['url']);
            this.postComment(message, fileUrl);
            this.showWebcam = false ;
        });  
    }
  }

  imageToFileConvert() {
    // Base64 url of image
    var base64=this.webcamImage['_imageAsDataUrl'].split("base64,");
    // Naming the image
    const date = new Date().valueOf();
    let text = '';
    const possibleText = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 5; i++) {
       text += possibleText.charAt(Math.floor(Math.random()*possibleText.length));
    }
    // Replace extension according to your media type
    const imageName = date + '.' + text + '.jpeg';
    console.log('Image name', imageName);
    // call method that creates a blob from dataUri
    const imageBlob = this.dataURItoBlob(base64[1]);
    this.imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' });
  }

  dataURItoBlob(dataURI) {
   const byteString = atob(dataURI);
   const arrayBuffer = new ArrayBuffer(byteString.length);
   const int8Array = new Uint8Array(arrayBuffer);
     for (let i = 0; i < byteString.length; i++) {
       int8Array[i] = byteString.charCodeAt(i);
     }
   const blob = new Blob([arrayBuffer], { type: 'image/jpeg' });    
   return blob;
  }

  closeCamera(){
    this.ltmsPwaImageCaptureContainer.nativeElement.style.display = "none";
    this.showWebcam = false;
  }

  getFeedStatus(brandId) {
        let statusList = [];
        this.statusListService.getstatusList(brandId, (statusData) => {
            statusList = statusData;
            this.renderFeedStatus(statusList);

        })
  }
  
  //Render flag color
  renderFeedStatus(statusListData) {
      console.log(this.feedDetail['feed']);
      console.log(this.feedDetail['feed']['status_id_list'])
      var statusListId = this.feedDetail['feed']['status_id_list'][0];

      statusListData.forEach(status => {
              if (status['uuid'] === statusListId) {
                  this.statusName = status['name'];
                  this.statusColor = status['color']
                  // if(!(status['color'])){
                  //    switch (status['name']) {
                  //      case "Open":
                  //          this.ltmsPwaFeedStatus.nativeElement.style.color = "#c09853";
                  //       break;
                  //      case "In-progress":
                  //            this.ltmsPwaFeedStatus.nativeElement.style.color = "#5a5a5a";
                  //        break;
                  //      case "On-hold":
                  //            this.ltmsPwaFeedStatus.nativeElement.style.color = "#b94a48";
                  //        break;
                  //      case "Closed":
                  //         this.ltmsPwaFeedStatus.nativeElement.style.color = "#468847";
                  //        break;
                  //      default:
                  //         break;
                  //    }
                  if (!this.statusColor) {
                      switch (status['name']) {
                          case "Open":
                              this.statusColor = "#f73e5a";
                              break;
                          case "In-progress":
                              this.statusColor = "#ffba00";
                              break;
                          case "On-hold":
                              this.statusColor = "#ffba00";
                              break;
                          case "Closed":
                              this.statusColor = "#6ac389";
                              break;
                          default:
                              break;
                      }
                  }

              } else {
                  // this.ltmsPwaFeedStatus.nativeElement.style.color = status['color'];
              }
      });
  }
}