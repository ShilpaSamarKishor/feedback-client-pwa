import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Location} from '@angular/common';
import { FeedDetailService} from "../feed-services/feed-detail.service";

@Component({
  selector: 'app-feed-header',
  templateUrl: './feed-header.component.html',
  styleUrls: ['./feed-header.component.css']
})
export class FeedHeaderComponent implements OnInit {
	  @ViewChild("ltmsPwaFeedBackButton") ltmsPwaFeedBackButton: ElementRef;
	  @Input() ltmsPwaFeedBackButton1: ElementRef

  constructor(private router: Router, private location: Location, private feedDetailService  :FeedDetailService) { }

  ngOnInit() {
    //Not navigate feeddetail page directly
  	//this.router.navigate(['/feed-list']);
    this.ltmsPwaFeedBackButton.nativeElement.style.display = "none";
    this.feedDetailService.subject.subscribe((data) => {
    	if(data['currentPage'] == "feed-detail"){
    		this.ltmsPwaFeedBackButton.nativeElement.style.display = "block";	
    	}
    })
  }

//GO to previous page
  goback(){
  	 this.location.back();
  	 this.ltmsPwaFeedBackButton.nativeElement.style.display = "none";
  }

//Redirect the Home page
  redirectToHome(){
    this.router.navigateByUrl('/home');
  }

}
