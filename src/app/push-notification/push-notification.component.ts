import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { Location } from '@angular/common';
import { UserService } from '../feed-services/user.service';
import 'src/assets/js/toast.js';

declare var Notification: any;
declare var toast : any;

@Component({
  selector: 'app-push-notification',
  templateUrl: './push-notification.component.html',
  styleUrls: ['./push-notification.component.css']
})

export class PushNotificationComponent implements OnInit {
 	 @ViewChild("ltmsPwaPushNotification") ltmsPwaPushNotification: ElementRef;
     @ViewChild("ltmsPwaPushNotificationImage") ltmsPwaPushNotificationImage:ElementRef

     readonly VAPID_PUBLIC_KEY = "BLbP49d3f9w8pRYl0P7Lt5WfIhUoP60v4saeQroCgWd6AVdPJhSyIQn-rJ18OB9JvepSABbj7y9jWpkdj1V-6VE";

     constructor(private userService : UserService, private location: Location) { }

  	ngOnInit() {
  		this.isPushSupported();
 	 }
       //To check `push notification` is supported or not
    	isPushSupported() {
        //To check `push notification` permission is denied by user
        if (Notification.permission === 'denied') {
            console.warn('User has blocked push notification.');
            return;
        }

        //Check `push notification` is supported or not
        if (!('PushManager' in window)) {
            console.error('Push notification isn\'t supported in your browser.');
            return;
        }

        //Get `push notification` subscription
        //If `serviceWorker` is registered and ready

        navigator.serviceWorker.ready
            .then((registration)=> {
                registration.pushManager.getSubscription()
                    .then((subscription) =>{
                        //If already access granted, enable push button status
                         if (subscription) {
                             this.changePushStatus(true);

                         } else {
                             this.changePushStatus(false);
                         }
                    })
                    .catch(function(error) {
                        console.error('Error occurred while enabling push ', error);
                    });
            });
    }


    //To change status
    changePushStatus(status) {
    	this.ltmsPwaPushNotification.nativeElement.dataset.checked = status;
       	this.ltmsPwaPushNotification.nativeElement.checked = status
        if (status) {
           this.ltmsPwaPushNotification.nativeElement.classList.add('active');
           this.ltmsPwaPushNotification.nativeElement.style.display = 'none';
           //this.ltmsPwaPushNotificationImage.nativeElement.src = './assets/image/push-on.png';
        } else {
            this.ltmsPwaPushNotification.nativeElement.classList.remove('active');
            this.ltmsPwaPushNotificationImage.nativeElement.src = './assets/image/push-off.png';
        }
    }


    urlB64ToUint8Array(base64String) {
        const padding = '='.repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding)
            .replace(/\-/g, '+')
            .replace(/_/g, '/');

        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);

        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
    }


    //To subscribe `push notification`
    subscribePush() {
        navigator.serviceWorker.ready.then((registration)=>{
            console.log('hye subscribe calll');
            if (!registration.pushManager) {
                alert('Your browser doesn\'t support push notification.');
                return false;
            }
            //To subscribe `push notification` from push manager
            registration.pushManager.subscribe({
                    userVisibleOnly: true,
                    applicationServerKey: this.urlB64ToUint8Array(this.VAPID_PUBLIC_KEY)
                })
                .then((subscription)=>{
                    console.log('hye subscribe calll');
                    toast('Subscribed successfully.');
                    this.changePushStatus(true);
                    this.sendPushNotification();
                })
                .catch((error)=>{
                    this.changePushStatus(false);
                    console.error('Push notification subscription error: ', error);
                });
        })
    }

    //To unsubscribe `push notification`
    unsubscribePush() {
        navigator.serviceWorker.ready
            .then((registration)=> {
                //Get `push subscription`
                registration.pushManager.getSubscription()
                    .then((subscription)=>{
                        if (!subscription) {
                            console.error('Unable to unregister push notification.');
                            return;
                        }

                        //Unsubscribe `push notification`
                        subscription.unsubscribe()
                            .then(()=>{
                            	toast('Unsubscribed successfully.');
                                console.info('Push notification unsubscribed.');
                                this.changePushStatus(false);
                            })
                            .catch((error) =>{
                                console.error(error);
                            });
                    })
                    .catch((error) =>{
                        console.error('Failed to unsubscribe push notification.');
                    });
            })
    }

    //Form data with info to send to server
    sendPushNotification() {
        navigator.serviceWorker.ready
            .then((registration)=>{
                //Get `push subscription`
                registration.pushManager.getSubscription().then((subscription)=>{
                    // this.pushNotificationService.pushSubscriptionKeyObject(JSON.stringify(subscription), function() {
                    // });

                    this.userService.updateUserPushSubscription(subscription).subscribe((response)=>{
                          console.log(response);
                    });
                })
            })
    }

    checkEventForSubscribe(){
    	 var isSubscribed = (this.ltmsPwaPushNotification.nativeElement.dataset.checked === 'true');
        if (isSubscribed) {
            this.unsubscribePush();
        } else {
            this.subscribePush();
        }
    }

}
