import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocalStorageService } from './localstorage.service';


@Injectable({
  providedIn: 'root'
})

export class UserService {

    private baseUrl = "https://staging.litmusworld.com/rateus/api/users/by_id?user_id={{user_id}}";
    private subscriptionUpdateUrl = "https://staging.litmusworld.com/rateus/api/users/update_new2?user_id={{user_id}}";
    
    constructor(private http: HttpClient,
      private localStorageService : LocalStorageService) { }
    
    // Get Respone from Server
    getUserDetails(): Observable<any[]>{
        
       let userId = this.localStorageService.getValue("user_id");
       this.baseUrl = this.baseUrl.replace('{{user_id}}', userId);
       
       return this.http.get<any[]>(this.baseUrl).pipe(map(response => {
         if(response && response['code'] == 0){
           this.localStorageService.setValue("user" , JSON.stringify(response['data']['user']));
           return response['data'];
         }else{
           return [];
         }      
       }));
    }

     // Get Respone from Server
    updateUserPushSubscription(subscriptionObject): Observable<any[]>{
        
       let userId = this.localStorageService.getValue("user_id");
       this.subscriptionUpdateUrl = this.subscriptionUpdateUrl.replace('{{user_id}}', userId);
       let brandId = this.localStorageService.getValue("brand_id");
       
       let body = {
           "subscription_date" : new Date(),
           "subscription_object" : subscriptionObject,
           "user_id" : userId,
           "brand_id" : brandId,
       }
       return this.http.put<any[]>(this.subscriptionUpdateUrl, body).pipe(map(response => {
         if(response && response['code'] == 0){
           return response['data'];
         }else{
           return [];
         }      
       }));
    }
}
