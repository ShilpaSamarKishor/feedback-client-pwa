import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {  LocalStorageService } from "./localstorage.service"

@Injectable({
  providedIn: 'root'
})
export class GenerateRequestService {

  private baseUrl = "https://staging.litmusworld.com/rateus/api";

  constructor(private http : HttpClient,
  private localStorageService : LocalStorageService) { }

  userProfile;

  ngOnInit() {
  }

  getFeedbackRequestUrl(app_id, cb){
    this.userProfile =  this.localStorageService.getValue("user");
    if(this.userProfile){
        this.userProfile = JSON.parse(this.userProfile);
        let Url = this.baseUrl + "/feedbackrequests/generate_customer_feedback_url";
        this.userProfile.user_phone = this.userProfile.phone_number;
        this.userProfile.user_email = this.userProfile.email_address;
        this.userProfile.is_generate_short_url = false;
        this.userProfile.app_id = app_id;
        this.userProfile.notify_channel = "none";
        this.http.post(Url, this.userProfile).subscribe(response => {
          cb(response);
        });
    }
  }
}
