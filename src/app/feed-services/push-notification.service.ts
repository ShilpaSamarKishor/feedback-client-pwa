import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class PushNotificationService {

  constructor(private http: HttpClient) { }
    
  pushSubscriptionKeyObject(subscriptionKey, cb){
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    var Url = 'http://localhost:4000/subscription_post';
  	return this.http.post(Url, JSON.parse(subscriptionKey),{headers}).subscribe(Response => {
  		cb();
  	})
  }

  getSubcriptionKeyObject(){
    var Url = 'http://localhost:4000/subscription_get'
    return this.http.get(Url).subscribe(Response => {
    })
  }
}


    