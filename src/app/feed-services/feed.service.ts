import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocalStorageService } from "../feed-services/localstorage.service";

@Injectable({
  providedIn: 'root'
})

export class FeedService {

    private baseUrl = "https://staging.litmusworld.com/rateus/api/feeds/list?brand_id={{brand_id}}&user_id={{user_id}}";
    private tokenDetailUrl = "https://staging.litmusworld.com/rateus/api/retrieve_feedback_context_shop?feedback_request_token={{feedback_request_token}}";
    
    constructor(private http: HttpClient,
    private localStorageService : LocalStorageService) { }
     // Get Respone from Server
     
    getFeedBacks(): Observable<any[]>{
       
      this.baseUrl = this.baseUrl.replace('{{brand_id}}', this.localStorageService.getValue("brand_id"));
      this.baseUrl = this.baseUrl.replace('{{user_id}}', this.localStorageService.getValue("user_id"));
       
      return this.http.get<any[]>(this.baseUrl).pipe(map(response => {
         console.log(response);
       if(response && response['code'] == 0){
         return response['data'];
       }
       return [];
     }));
    }

    getRequestDetailsByToken() : Observable<any[]>{
        this.tokenDetailUrl = this.tokenDetailUrl.replace('{{feedback_request_token}}', this.localStorageService.getValue("feedback_request_token"));
        return this.http.get<any[]>(this.tokenDetailUrl).pipe(map(response => {
           console.log(response); 
         if(response && response['code'] == 0){
           return response['data']['feedback_request_details'];
         }
         return [];
       }));
    }
}


