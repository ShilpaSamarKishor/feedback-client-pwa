import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class FileAttachmentService {

  constructor(private httpClient : HttpClient) { }

  public baseUrl = "https://staging.litmusworld.com/rateus"
  //https://staging.litmusworld.com/rateus/api/uploaders/multimedia_file_upload
  getFileAttachmentUrl(selectedFile, cb){
      console.log(',,,,,,,,,,,,,,,,,,,,', selectedFile);
     var formData = new FormData();
     formData.append("file", selectedFile);
     if(selectedFile && selectedFile.type){
       var splitFileType = selectedFile.type.split('/');
       formData.append("file_type", splitFileType[1]);
     }else{
      formData.append("file_type", selectedFile.name.split('.')[1]);
     }
     var fileUploadUrl = this.baseUrl + "/api/uploaders/multimedia_file_upload";
     return this.httpClient.post(fileUploadUrl, formData).subscribe(response => {
        cb(response);
    })
  }
}