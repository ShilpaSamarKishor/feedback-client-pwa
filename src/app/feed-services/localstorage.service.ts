import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class LocalStorageService {

    constructor() {}

    setValue(key, value) {
    	localStorage.setItem(key, value);
    }

    getValue(key){
    	return localStorage.getItem(key);
    }
}
