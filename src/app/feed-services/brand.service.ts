import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocalStorageService } from './localstorage.service';


@Injectable({
  providedIn: 'root'
})

export class BrandService {

    private baseUrl = "https://staging.litmusworld.com/rateus/api/brands/by_id?brand_id={{brand_id}}";
    
    constructor(private http: HttpClient,
      private localStorageService : LocalStorageService) { }
    
    // Get Respone from Server
    getBrandDetails(): Observable<any[]>{
        
       let brandId = this.localStorageService.getValue("brand_id");
       this.baseUrl = this.baseUrl.replace('{{brand_id}}', brandId);
       
       return this.http.get<any[]>(this.baseUrl).pipe(map(response => {
         if(response && response['code'] == 0){
           this.localStorageService.setValue("brand" , JSON.stringify(response['data']['brand']));
           return response['data'];
         }else{
           return [];
         }      
       }));
    }
}
