import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FeedDetailService {
	selectedFeed =  null;
	private baseUrl = "https://staging.litmusworld.com/rateus";
  subject = new Subject<Object>();

  constructor(private http: HttpClient) { }
  
  getFeedDetailById(feedId : String ):Observable<any[]>{
  	let url =  this.baseUrl + '/api/feed_details_by_id?feed_id=' + feedId;
  	return this.http.get<any[]>(url).pipe(map(response => {
      this.selectedFeed =  response['data'];
      var feedBackId = response['data']['feed']['feedback_id'];
      return this.selectedFeed;
    }));
  }
}
