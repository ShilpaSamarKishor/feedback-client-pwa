import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject} from 'rxjs';
import { LocalStorageService } from "../feed-services/localstorage.service";


@Injectable({
  providedIn: 'root'
})
export class AttachmentPreviewService {

  constructor(private http: HttpClient, private localStorageService : LocalStorageService) { }

  private baseBarandUrl = "https://staging.litmusworld.com/rateus/api/infra/url?brand_id={{brand_id}}";
  private baseUrl = 'https://staging.litmusworld.com/rateus/api/contents/file?';

  	getUrlForReadAttachFile(fileInfo : String, cb){
  	  	this.baseBarandUrl = this.baseBarandUrl.replace('{{brand_id}}', this.localStorageService.getValue("brand_id"));

  	  //API url for the download and Preview attached file
  		//https://staging.litmusworld.com/rateus/api/infra/url?brand_id=5b4ed5891e83406bdaf56849&file_url=contents%2Fimage%2Fcolor_1542715409841.png&type=1
    	//https://staging.litmusworld.com/rateus/api/contents/file?file_id=5bbda108c1d948603d8087c9
  	 	if (fileInfo != null && typeof fileInfo === 'string' && fileInfo.length === 24 && typeof fileInfo != undefined){
  	 		let url = this.baseUrl + 'file_id=' + fileInfo
  	 		return this.http.get<any>(url).subscribe(response => {
  	 			if(response && response['code'] == 0){
         			cb(response['data']);
       			}
  	 			else 
  	 				cb('Empty Response');
    		});
  	 	}
  	 	else{
  	 		let url = this.baseBarandUrl + '&file_url=' + fileInfo + '&type=1';
  	 		return this.http.get<any>(url).subscribe(response => {
  	 			if(response && response['code'] == 0){
         			cb(response['data']);
       			}
  	 			else 
  	 				cb('Empty Response');
    		});
  	 	}
  	}
}


