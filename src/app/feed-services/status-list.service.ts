import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class StatusListService {

 	private baseUrl = "https://staging.litmusworld.com/rateus";
 	private url = "/api/brands/status_list?";

  constructor(private http: HttpClient) { }

  // Get status list from server
  getStatusListApi(brand_id): Observable<any[]>{
    let Url = this.baseUrl + this.url + 'brand_id=' + brand_id ;
    return this.http.get<any[]>(Url).pipe(map(response => {
      if(response && response['code'] == 0){
        return response['data']['status_list'];
      }
       return [];
    }));
  }

//Store status list on map
    private statusListArray = [];
    private statusListMap : Map<String, any[]> = new Map<String, any[]>();
    getData(brand_id, statusList){
      statusList.forEach(status=> {
         this.statusListArray.push(status)
      });
         this.statusListMap.set(brand_id , this.statusListArray);
    }

    //Return status list to component 
    getstatusList(brand_id, cb){
      if(brand_id in this.statusListMap)
      {
          var status = this.statusListMap.get(brand_id);
          cb(status);
      }else{
          this.getStatusListApi(brand_id).subscribe(status =>{
             this.getData(brand_id, status);
             this.statusListMap.set(brand_id , this.statusListArray);
             cb(status);
          }) 
      }
    }
}
