import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class PostMessageService {

private baseUrl = "https://staging.litmusworld.com/rateus";

  constructor(private http: HttpClient) { }

  	//Post New Message 
    postComment(messages, cb){
        var postMessageurl = this.baseUrl + '/api/comment';
		return this.http.post(postMessageurl, messages).subscribe(response => {
			cb();
		});
	}
}


