import { TestBed } from '@angular/core/testing';
import { AttachmentPreviewService } from './attachment-preview.service';

describe('AttachmentPreviewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AttachmentPreviewService = TestBed.get(AttachmentPreviewService);
    expect(service).toBeTruthy();
  });
});
