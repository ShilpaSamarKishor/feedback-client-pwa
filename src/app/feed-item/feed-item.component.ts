import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Router } from "@angular/router";
import { StatusListService } from '../feed-services/status-list.service';
import 'src/assets/js/mini-rater-chat.js';

declare var MiniRater: any;
declare var NPSMiniRater: any;


@Component({
    selector: 'app-feed-item',
    templateUrl: './feed-item.component.html',
    styleUrls: ['./feed-item.component.css']
})

export class FeedItemComponent implements OnInit {

    @ViewChild("primaryRating") primaryRating:ElementRef;
    //@ViewChild("ltmsPwaFeedDeviceContainer") ltmsPwaFeedDeviceContainer:ElementRef;
    @ViewChild("ltmsPwaFeedStatus") ltmsPwaFeedStatus: ElementRef;

    feed: any;
    data = [];
    statusColor = "black";
    statusName = "Open"


    constructor(private router: Router, private statusListService: StatusListService) {}

    @Input() feedItem;

    ngOnInit() {
        this.getMiniRater();
        this.getFeedStatus(this.feedItem['brand_id']);
    }

    //Rendering rater for primary rating 
    getMiniRater() {
        this.primaryRating.nativeElement.id = this.feedItem['_id']
        this.primaryRating.nativeElement.style.boxShadow = "0 3px 6px 0 rgba(0, 0, 0, 0.12)";
        this.primaryRating.nativeElement.style.borderRadius = "42px";
        this.primaryRating.nativeElement.style.padding = "3px";
        //this.ltmsPwaFeedDeviceContainer.nativeElement.className = this.feedItem['className']
        this.renderMiniRater(this.feedItem, this.feedItem['_id'])

    }

    renderMiniRater(feed, DomStr) {
        var ratingWidget = null;
        var raterType = feed["main_rater_type"];
        if (raterType == 'nps') {
            ratingWidget = new NPSMiniRater(DomStr);
        } else {
            ratingWidget = new MiniRater(DomStr);
        }
        ratingWidget.draw();
        var rating = 0;
        if(feed && feed.feed_params && feed.feed_params.primary_rating){
          rating =  feed.feed_params['primary_rating'];
        }
        ratingWidget.setRating(rating);
    }

    //Get status list for api and render the flag base on status name
    getFeedStatus(brandId) {
        let statusList = [];
        this.statusListService.getstatusList(brandId, (statusData) => {
            statusList = statusData;
            this.renderFeedStatus(statusList);

        })
    }

    

    //Render flag color
    renderFeedStatus(statusListData) {
        console.log(this.feedItem);
        console.log(this.feedItem['status_id_list'])
        var statusListId = this.feedItem['status_id_list'][0];

        statusListData.forEach(status => {
                if (status['uuid'] === statusListId) {
                    this.statusName = status['name'];
                    this.statusColor = status['color']
                    // if(!(status['color'])){
                    //    switch (status['name']) {
                    //      case "Open":
                    //          this.ltmsPwaFeedStatus.nativeElement.style.color = "#c09853";
                    //       break;
                    //      case "In-progress":
                    //            this.ltmsPwaFeedStatus.nativeElement.style.color = "#5a5a5a";
                    //        break;
                    //      case "On-hold":
                    //            this.ltmsPwaFeedStatus.nativeElement.style.color = "#b94a48";
                    //        break;
                    //      case "Closed":
                    //         this.ltmsPwaFeedStatus.nativeElement.style.color = "#468847";
                    //        break;
                    //      default:
                    //         break;
                    //    }
                    if (!this.statusColor) {
                        switch (status['name']) {
                            case "Open":
                                this.statusColor = "#f73e5a";
                                break;
                            case "In-progress":
                                this.statusColor = "#ffba00";
                                break;
                            case "On-hold":
                                this.statusColor = "#ffba00";
                                break;
                            case "Closed":
                                this.statusColor = "#6ac389";
                                break;
                            default:
                                break;
                        }
                    }

                } else {
                    // this.ltmsPwaFeedStatus.nativeElement.style.color = status['color'];
                }
        })
    }
}