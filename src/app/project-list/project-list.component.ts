import { Component, OnInit } from '@angular/core';
import { GenerateRequestService } from '../feed-services/generate-request.service';
import { Router } from '@angular/router';
import { ServiceRequestComponent } from '../service-request/service-request.component';
import { ShopService } from '../feed-services/shop.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})

export class ProjectListComponent implements OnInit {
  constructor(private router : Router,
  private shopService : ShopService, private spinner: NgxSpinnerService) { }
  public shops = [];
  ngOnInit() {
      this.spinner.show();
      this.getShopData();      
  }

  getShopData(){
    this.shopService.getShops().subscribe(data => {
         this.shops= data;
         this.spinner.hide();
      });
  }

}
