import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FeedListComponent } from './feed-list/feed-list.component';
import { FeedItemComponent } from './feed-item/feed-item.component';
import { FeedDetailComponent } from './feed-detail/feed-detail.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { FeedService } from './feed-services/feed.service';
import { UserService } from './feed-services/user.service';
import { ShopService } from './feed-services/shop.service';
import { BrandService } from './feed-services/brand.service';
import { LocalStorageService } from './feed-services/localstorage.service';
import { EditUserProfileComponent } from './edit-user-profile/edit-user-profile.component';
import { ServiceRequestComponent } from './service-request/service-request.component';
import { ProjectItemComponent } from './project-item/project-item.component';
import { Injectable } from '@angular/core';

const routes: Routes = [
  { path: '', redirectTo: '/feed-list', pathMatch: 'full' },
  { path: 'home', component: FeedListComponent },
  { path: 'edit-user-profile', component: EditUserProfileComponent },
  { path: 'feed-list', component: FeedListComponent },
  { path: 'feed-detail/:feedId', component: FeedDetailComponent},
  { path: 'project-list', component: ProjectListComponent},
  { path: 'service-request/:app_id', component: ServiceRequestComponent},
  { path: 'project-item', component:ProjectItemComponent },
  { path: '**', component: FeedListComponent },
]; 

@NgModule({
   exports: [ RouterModule ],
   imports: [ RouterModule.forRoot(routes) ]
})

export class AppRoutingModule {
 }
