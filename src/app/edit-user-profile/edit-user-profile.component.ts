import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LocalStorageService } from "../feed-services/localstorage.service";
import { FileAttachmentService } from '../feed-services/file-attachment.service';
import { AttachmentPreviewService } from '../feed-services/attachment-preview.service';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { Subject } from 'rxjs';
import { Observable }from 'rxjs';



@Component({
  selector: 'app-edit-user-profile',
  templateUrl: './edit-user-profile.component.html',
  styleUrls: ['./edit-user-profile.component.css']
})
export class EditUserProfileComponent implements OnInit {
  @ViewChild("editUserProfileForm") editUserProfileForm : NgForm;
  @ViewChild("ltmsPwaProfilePicMainContainer") ltmsPwaProfilePicMainContainer : ElementRef;
  @ViewChild("ltmsPwaprofilePicAttachment") ltmsPwaprofilePicAttachment : ElementRef;
  @ViewChild("ltmsPwaProfilePicResetContainer") ltmsPwaProfilePicResetContainer : ElementRef;
  @ViewChild("ltmsPwaPictureCameraCaptureContainer") ltmsPwaPictureCameraCaptureContainer : ElementRef;


  submitted = false;
  public profilePicUrl ;
  public userProfileInfo ;


  //Web camera
  public showWebcam = false;
  public allowCameraSwitch = true;
  public multipleWebcamsAvailable = false;
  public deviceId: string;
  public imageFile;
  public flag : boolean;

  public errors: WebcamInitError[] = [];
  // latest snapshot
  public webcamImage: WebcamImage = null;
  // webcam snapshot trigger
  private trigger: Subject<void> = new Subject<void>();
  // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
  private nextWebcam: Subject<boolean|string> = new Subject<boolean|string>();
  constructor(private localStorageService : LocalStorageService, private fileAttachmentService : FileAttachmentService, 
    private attachmentPreviewService : AttachmentPreviewService) { }

  userProfile;

  ngOnInit() {
     this.submitted = false;
     this.userProfile =  this.localStorageService.getValue("user");
     this.userProfileInfo = this.localStorageService.getValue("userProfilePic");
       if(this.userProfile){
           this.userProfile = JSON.parse(this.userProfile);
       }
  }

  //Form data submit and call API to store edited user profile data
  onSubmit() { this.submitted = false; 
  }

  //Reset the form value 
  resetUserProfile(){
       let data = this.getUserProfileData()
       this.editUserProfileForm.resetForm(data); 
       this.submitted=false; 
  }

  //Call API to get user Profile data
  getUserProfileData(){
    return this.userProfile;
  }  
  //call for Profile pic selection
  getProfilePic() {
    this.ltmsPwaProfilePicMainContainer.nativeElement.style.display = "block";
  }
  //take Profile pic from folder
  getProfilePicFromlocal() {
    this.ltmsPwaProfilePicMainContainer.nativeElement.style.display = 'none';
    this.ltmsPwaprofilePicAttachment.nativeElement.click();
  }

  getFileAttachment(event) {
    this.flag = true;
    if(event.target.files && event.target.files.length){
          this.fileAttachmentService.getFileAttachmentUrl(event.target.files[0] , (res)=> {
            this.attachmentPreviewService.getUrlForReadAttachFile(res['data']['url'] ,(previewUrl)=> {
              this.profilePicUrl = previewUrl['file_url'];
              if(this.profilePicUrl){
                this.ltmsPwaProfilePicResetContainer.nativeElement.style.display = 'block';
              }
             });
        });   
    }
  }

  setProfilePic() {
    this.ltmsPwaProfilePicResetContainer.nativeElement.style.display = 'none';
    this.localStorageService.setValue("userProfilePic" , this.profilePicUrl);
  }

  resetProfilePic(event) {
    if(this.flag == true){
          //this.ltmsPwaProfilePicResetContainer.nativeElement.style.display = 'none';
          this.ltmsPwaprofilePicAttachment.nativeElement.click();
    }else
    {
          //this.ltmsPwaProfilePicResetContainer.nativeElement.style.display = 'none';
           this.showWebcam = true;
           this.ltmsPwaPictureCameraCaptureContainer.nativeElement.style.display = 'block';
    }
  }

  //Web Camera Function
  //Take Profile pic from camera 
  public takeProfilePicByCamera() {
    this.flag = false;
     WebcamUtil.getAvailableVideoInputs()
        .then((mediaDevices: MediaDeviceInfo[]) => {
          this.showWebcam = true;
          this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
          this.ltmsPwaPictureCameraCaptureContainer.nativeElement.style.display = 'block';   
     });
  }
  public videoOptions: MediaTrackConstraints = {
    width: {ideal: 1024},
    height: {ideal: 576}
  };
  public triggerSnapshot(): void {
    this.trigger.next();
    this.imageToFileConvert();
  }

  public handleInitError(error: WebcamInitError): void {
    this.errors.push(error);
  }

  public showNextWebcam(directionOrDeviceId: boolean|string): void {
    // true => move forward through devices
    // false => move backwards through devices
    // string => move to device with given deviceId
    this.nextWebcam.next(directionOrDeviceId);
  }
  //Push capture image in array
  public handleImage(webcamImage: WebcamImage): void {
    this.webcamImage = webcamImage;
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>',this.webcamImage);
  }

  public cameraWasSwitched(deviceId: string): void {
    this.deviceId = deviceId;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean|string> {
    return this.nextWebcam.asObservable();
  }

  public closeCameraCapture() {
    this.showWebcam = false;
    this.ltmsPwaPictureCameraCaptureContainer.nativeElement.style.display = 'none';   
  }

   imageToFileConvert() {
    // Base64 url of image
    var base64=this.webcamImage['_imageAsDataUrl'].split("base64,");
    // Naming the image
    const date = new Date().valueOf();
    let text = '';
    const possibleText = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 5; i++) {
       text += possibleText.charAt(Math.floor(Math.random()*possibleText.length));
    }
    // Replace extension according to your media type
    const imageName = date + '.' + text + '.jpeg';
    console.log('Image name', imageName);
    // call method that creates a blob from dataUri
    const imageBlob = this.dataURItoBlob(base64[1]);
    this.imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' });
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>',this.imageFile);
    if(this.imageFile){
      this.showWebcam = false;
      this.ltmsPwaPictureCameraCaptureContainer.nativeElement.style.display = 'none';
      this.fileAttachmentService.getFileAttachmentUrl(this.imageFile , (res)=> {
            this.attachmentPreviewService.getUrlForReadAttachFile(res['data']['url'] ,(previewUrl)=> {
              this.profilePicUrl = previewUrl['file_url'];
              if(this.profilePicUrl){
                this.ltmsPwaProfilePicResetContainer.nativeElement.style.display = 'block';
              }
             });
        });  
    }
  }

  dataURItoBlob(dataURI) {
   const byteString = atob(dataURI);
   const arrayBuffer = new ArrayBuffer(byteString.length);
   const int8Array = new Uint8Array(arrayBuffer);
     for (let i = 0; i < byteString.length; i++) {
       int8Array[i] = byteString.charCodeAt(i);
     }
   const blob = new Blob([arrayBuffer], { type: 'image/jpeg' });    
   return blob;
  }

}
